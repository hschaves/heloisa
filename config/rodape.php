  <footer class="page-footer mdb-color lighten-3 text-center text-md-left">

    <div class="container">

      <div class="row">
        <div class="col-md-12">

          <h5 class="my-5 d-flex justify-content-center">Contato: heloisa.chaves@aluno.ifsp.edu.br</h5>
        </div>
      </div>
    </div>

    <div class="footer-copyright text-center py-3 wow fadeIn" data-wow-delay="0.3s">
      <div class="container-fluid">
        &copy; 2021 Copyright:
        <a href="https://mdbootstrap.com/docs/jquery/"> Heloisa Chaves </a>
      </div>
    </div>

  </footer>