  <main>

    <div class="container">

      <section class="text-center team-section">

        <div class="row text-center">

          <div class="col-md-12 mb-4" style="margin-top: -100px;">

            <div class="avatar mx-auto">
              <img src="mdb/img/photo02.jpg" class="img-fluid rounded-circle z-depth-1"
                alt="First sample avatar image">
            </div>
            <h3 class="my-3 font-weight-bold">
              <strong>Heloisa Chaves</strong>
            </h3>
            <h6 class="font-weight-bold pink-text mb-4">Linguagem de Programação I</h6>

            <a class="p-2 m-2 fa-lg fb-ic">
              <i class="fab fa-facebook-f grey-text"> </i>
            </a>
            <a class="p-2 m-2 fa-lg tw-ic">
              <i class="fab fa-twitter grey-text"> </i>
            </a>
            <a class="p-2 m-2 fa-lg ins-ic">
              <i class="fab fa-instagram grey-text"> </i>
            </a>

            <p class="mt-5">
                Estudante do 2° semestre de Analise
                Denvolvimento de Sistemas no
                IFSP - Câmpus Guarulhos</p>
          </div>

        </div>

      </section>

      <section>

        <ul class="nav md-pills pills-default d-flex justify-content-center">
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#panel11" role="tab">
              <strong>MAIS SOBRE MIM</strong>
            </a>
          </li>
        </ul>

        <div class="tab-content">

          <div class="tab-pane fade  show active" id="panel11" role="tabpanel">
            <br>

            <div class="row">

              <div class="col-md-12">

                <section class="text-center mb-5">

                  <div class="row mb-4">

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(41).jpg');">

                        <div class="text-white text-center d-flex align-items-center rgba-blue-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Meus Hobbies</strong>
                            </h3>
                            <p>No meu tempo livre, gosto de tocar violão.<br> Toco na igreja com meus amigos no segundo domingo de cada mês. </br>Amo passear a minha 
                                familia e amigos e sair pra comer.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(14).jpg');">

                        <div class="text-white text-center d-flex align-items-center rgba-teal-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Robótica</strong>
                            </h3>
                            <p>Participei 3 anos da equipe de robótica do meu antigo colégio.
                             Nesse período participei de diversas competições, onde fui campeã nacional, regional e estadual na modalidade resgate 
                             no plano com a minha equipe.</p>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(11).jpg');">

                        <div class="text-white text-center d-flex align-items-center rgba-green-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Viagens</strong>
                            </h3>
                            <p>Eu amo viajar e conhecer novas culturas. Em 2019, fiz um intercâmbio para Boston,
                              onde passei três semanas estudando inglês e conhecendo pessoas de diversas
                              culturas. Foi uma experiência muito importante para mim</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 mb-4">
                      <div class="card card-image" style="background-image: url('https://mdbootstrap.com/img/Photos/Horizontal/Work/6-col/img%20(42).jpg');">

                        <div class="text-white text-center d-flex align-items-center rgba-stylish-strong py-5 px-4">
                          <div>
                            <h3 class="mb-4 mt-4 font-weight-bold">
                              <strong>Trabalho</strong>
                            </h3>
                            <p>Atualmente, estou trabalhando como Jovem aprendiz de ITSM na Karina
                             Plásticos, onde atuo no ServiceDesk. 
                             Estou a procura de um estágio na área de TI, para que eu possa aplicar
                             as habilidades que estou adquirindo na faculdade.</p>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                </section>

              </div>

            </div>

          </div>

          <div class="tab-pane fade" id="panel13" role="tabpanel">
            <br>

            <div class="row d-flex justify-content-center">
              <div class="col-md-12">

                <div id="mdb-lightbox-ui"></div>

                <div class="mdb-lightbox no-gutters">

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(71).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(71).jpg" class="img-fluid">
                    </a>
                  </figure>

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(65).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(65).jpg" class="img-fluid" />
                    </a>
                  </figure>

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(84).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(84).jpg" class="img-fluid" />
                    </a>
                  </figure>

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(88).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(88).jpg" class="img-fluid" />
                    </a>
                  </figure>

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(79).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(79).jpg" class="img-fluid" />
                    </a>
                  </figure>

                  <figure class="col-md-4">
                    <a href="https://mdbootstrap.com/img/Mockups/Lightbox/Original/img%20(81).jpg" data-size="1600x1067">
                      <img src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(81).jpg" class="img-fluid" />
                    </a>
                  </figure>

                </div>

              </div>
            </div>

          </div>

        </div>

      </section>

    </div>

  </main>