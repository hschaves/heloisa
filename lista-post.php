<?php include 'config/cabecalho.php' ?>

<?php 
    include_once 'Aula07/Model/data-model.php';
    include_once 'Aula07/Model/blog-model.php';
    $introduction = introSection ('Dados dos Posts');
    include 'Aula07/View/header.php';
?>

<h1 class="text-center mt-3">Área de Edição de Posts</h1>

<?php
    $table = getPostTable() 
?>

<?php include 'Aula07/View/lista.php' ?>

<?php include 'Aula07/View/footer.php' ?>

<?php include 'config/rodape.php' ?>