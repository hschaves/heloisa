  <header>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar pink">
      <div class="container">
        <a class="navbar-brand" href="#">
          <strong>MEU PORTIFÓLIO</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
          aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Perfil
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
          <form class="form-inline">
            <div class="md-form my-0">
              <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            </div>
          </form>
        </div>
      </div>
    </nav>

    <div class="view jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient3.png); background-repeat: no-repeat; background-size: cover; background-position: center center;">
      <div class="mask rgba-indigo-slight">
        <div class="container h-100 d-flex justify-content-center align-items-center">
          <div class="row pt-5 mt-3">
            <div class="col-md-12 mb-3">
              <div class="intro-info-content text-center">
                <h1 class="display-3 pink-text mb-5 wow fadeInDown" data-wow-delay="0.3s">SOBRE
                  <a class="pink-text font-weight-bold">MIM</a>
                </h1>
                <h5 class="text-uppercase black-text mb-5 mt-1 font-weight-bold wow fadeInDown" data-wow-delay="0.3s">Matrícula: 301536X</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </header>