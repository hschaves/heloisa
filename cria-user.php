<?php include 'config/cabecalho.php' ?>

<?php
    include_once 'Aula07/Model/db-manager.php';
    insertUser($_POST);
?>

<!-- content -->

<div class="container"></div>
    <div class="row mt-4"> 
        <div class="col-md-5">
            <p class="alert alert-danger text-center">Usuário</p>
        </div>
        <div class="col-md-5">
            <p class="alert alert-primary" text-left><?= $_POST['user']?></p>
        </div>
    </div>   
    <div class="row mt-4">
        <div class="col-md-5">
            <p class="alert alert-danger text-center">Nome Completo</p>
        </div>
        <div class="col-md-5">
            <p class="alert alert-primary" text-left><?= $_POST['nome']?></p>
        </div>
    </div>    
    <div class="row mt-4">
        <div class="col-md-5">
            <p class="alert alert-danger text-center">E-mail</p>
        </div>
        <div class="col-md-5">
            <p class="alert alert-primary" text-left><?= $_POST['email']?></p>
        </div>
    </div>
</div>

<?php header ('Location: http://localhost/heloisa/Aula07.php'); ?>

<!-- content -->

<?php include 'Aula07/View/footer.php' ?>

<?php include 'config/rodape.php' ?>