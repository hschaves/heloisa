 <section class="section extra-margins pb-3 text-center text-lg-left wow fadeIn" data-wow-delay="0.3s">

<h2 class="font-weight-bold text-center h1 my-5">Noticias Recentes</h2>
<p class="text-center grey-text mb-5 mx-auto w-responsive">Aqui você encontra as noticias mais recentes!!</p>

  <?= getRecentsPosts($n) ?>