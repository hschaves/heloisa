<body>

  <header>

    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
      <div class="container">
        <a class="navbar-brand" href="#"><strong>Heloisa Chaves</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
          aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="http://localhost/heloisa/aula07.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/heloisa/login.php">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/heloisa/new-user.php">Novo Usuário</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/heloisa/new-post.php">Novo Post</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/heloisa/lista-post.php">Edite seu post</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="http://localhost/heloisa/lista-user.php">Edite seu usuário</a>
            </li>
          </ul>
          <form class="form-inline">
            <div class="md-form my-0">
              <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            </div>
          </form>
        </div>
      </div>
    </nav>

    <?php echo $introduction ?>

  </header>