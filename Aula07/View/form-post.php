<?php
    $editing = isset($data);
    $url = $editing ? 'edita-post.php' : 'cria-post.php';
?>

<div class="row"></div>
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" action="<?= $url ?>" method="POST">

            <p class="h4 mb-4"><?= $editing ? 'Edite seu post': 'Crie seu post'?></p>
            <input value="<?= $editing ? $data[0] : ''?>" type="text" id="imagem" name="imagem" class="form-control mb-4" placeholder="URL da Imagem">
            
            <?php if(! $editing):?>
                <input type="text" id="cor_categoria" name="cor_categoria" class="form-control mb-4" placeholder="Cor da Categoria">
            <?php endif ?>

            <input value="<?= $editing ? $data[2] : ''?>" type="text" id="categoria" name="categoria" class="form-control mb-4" placeholder="Categoria">
            <input value="<?= $editing ? $data[3] : ''?>" type="text" id="titulo" name="titulo" class="form-control mb-4" placeholder="Título">    
            <input value="<?= $editing ? $data[4] : ''?>" type="text" id="conteudo" name="conteudo" class="form-control mb-4" placeholder="Conteúdo">
            
            <?php if(!$editing):?>
                <input type="text" id="autor" name="autor" class="form-control mb-4" placeholder="Autor">
            <?php endif ?>
            
            <?php if(! $editing):?>
                <input type="date" id="data" name="data" class="form-control mb-4" placeholder="Data">
            <?php endif ?>

            <?php if($editing):?>
                <input type="hidden" name="id" value="<?= $id ?>">
            <?php endif ?>

            <button class="btn btn-info btn-block my-4" type="submit"><?= $editing ? 'Editar': 'Criar'?></button>

        </form>
    </div>
</div>
