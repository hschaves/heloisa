<?php
    $editing = isset($data);
    $url = $editing ? 'edita-user.php' : 'cria-user.php';
?>

<div class="row"></div>
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" action="<?= $url ?>" method="POST">

            <p class="h4 mb-4"><?= $editing ? 'Edite o usuário': 'Crie seu usuário para publicar em nosso site'?></p>
            <input value="<?= $editing ? $data[0] : ''?>" type="text" id="user" name="user" class="form-control mb-4" placeholder="User">
            <input value="<?= $editing ? $data[1] : ''?>" type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
            <input value="<?= $editing ? $data[2] : ''?>" type="text" id="email" name="email" class="form-control mb-4" placeholder="E-mail">    

            <button class="btn btn-info btn-block my-4" type="submit"><?= $editing ? 'Editar': 'Criar'?></button>

        </form>
    </div>
</div>