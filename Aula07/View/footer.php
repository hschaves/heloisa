<footer class="page-footer pt-4 mt-4   text-center text-md-left">

    <div class="text-center mb-3">

      <a class="btn-floating btn-fb"><i class="fab fa-facebook-f"> </i></a>
      <a class="btn-floating btn-li"><i class="fab fa-linkedin-in"> </i></a>


    </div>

  </footer>

  <script type="text/javascript" src="../mdb/js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="../mdb/js/popper.min.js"></script>
  <script type="text/javascript" src="../mdb/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../mdb/js/mdb.min.js"></script>
  <script>
    new WOW().init();

  </script>
</body>

</html>