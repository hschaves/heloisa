<!-- id, imagem, cor_categoria, categoria, titulo, conteudo, autor, data, created_at - Recent -->

<div class="container mt-6">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Imagem</th>
                <th scope="col">Categoria</th>
                <th scope="col">Título</th>
                <th scope="col">Conteúdo</th>
                <th scope="col">Ação</th>
            </tr>
        </thead>
        <tbody>
            <?= $table ?>
        </tbody>
    </table>
</div>