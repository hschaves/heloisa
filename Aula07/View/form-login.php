<?php
    $login = isset($data);
    $url = $login ? 'login-user.php' : 'cria-user.php';
?>

<div class="row"></div>
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" action="<?= $url ?>" method="POST">

            <p class="h4 mb-4">Login</p>
            <input value="<?= $login ? $data[0] : ''?>" type="text" id="user" name="user" class="form-control mb-4" placeholder="User">   

            <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

        </form>
    </div>
</div>