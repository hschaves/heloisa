<div class="container mt-6">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">User</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Ação</th>
            </tr>
        </thead>
        <tbody>
            <?= $table ?>
        </tbody>
    </table>
</div>