<?php include 'config/cabecalho.php' ?>

<?php
    include_once 'Aula07/Model/db-manager.php';
    insertPost($_POST);
?>

<!-- content -->

<div class="container"></div>
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Imagem</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['imagem']?></p>
        </div>
    </div>    
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Cor da Categoria</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['cor_categoria']?></p>
        </div>
    </div>   
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Categoria</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['categoria']?></p>
        </div>
    </div>    
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Título</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['titulo']?></p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Autor</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['autor']?></p>
        </div>
    </div>       
    <div class="row mt-5">
        <div class="col-md-6">
            <p class="alert alert-danger text-center">Data</p>
        </div>
        <div class="col-md-6">
            <p class="alert alert-primary" text-left><?= $_POST['data']?></p>
        </div>
    </div>   
</div>

<?php header ('Location: http://localhost/heloisa/Aula07.php'); ?>

<!-- content -->

<?php include 'Aula07/View/footer.php' ?>

<?php include 'config/rodape.php' ?>


